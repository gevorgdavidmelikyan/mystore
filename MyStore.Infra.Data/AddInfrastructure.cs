﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MyStore.Domain.Interfaces;
using MyStore.Infra.Data.Contexts;
using MyStore.Infra.Data.Interfaces;
using MyStore.Infra.Data.Repositories;

namespace MyStore.Infra.Data
{
    public static class DependencyContainer
    {
        public static void AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("Default")));


            services.AddMemoryCache();




            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddScoped<IRepository, Repository>();
            services.AddScoped<IApplicationDbContext, ApplicationDbContext>();

        }
    }

}