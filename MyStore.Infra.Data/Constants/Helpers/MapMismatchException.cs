﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyStore.Infra.Data.Constants.Helpers
{
    public class MapMismatchException : Exception
    {
        public MapMismatchException(string arg) : base(arg)
        {
        }
    }
}
