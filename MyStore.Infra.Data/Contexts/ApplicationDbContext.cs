﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using MyStore.Domain;
using MyStore.Domain.Entities;
using MyStore.Domain.Interfaces;
using MyStore.Infra.Data.Constants.Helpers;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace MyStore.Infra.Data.Contexts
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }

        public List<TView> Execute<TView>(string name, params KeyValuePair<string, object>[] parameters) where TView : class, new()
        {
            var list = new List<SqlParameter>();
            if (parameters != null)
                foreach (var keyValuePair in parameters)
                {
                    var param = new SqlParameter
                    {
                        Value = keyValuePair.Value,
                        ParameterName = keyValuePair.Key
                    };
                    if (param.DbType == DbType.Decimal)
                    {
                        param.Precision = 18;
                        param.Scale = 6;
                    }
                    list.Add(param);
                }
            return ExecuteQuery<TView>(this, name, list.ToArray()).ToList();
        }
        private IEnumerable<T> ExecuteQuery<T>(ApplicationDbContext context, string query, params SqlParameter[] sqlParams) where T : class, new()
        {
            using var command = context.Database.GetDbConnection().CreateCommand();
            command.CommandText = query;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandTimeout = 60;
            if (sqlParams != null)
            {
                command.Parameters.AddRange(sqlParams);
            }
            using (context.Database.OpenConnectionAsync())
            {
                using (var reader = command.ExecuteReader())
                {
                    if (!reader.Any())
                        yield break;
                    var mapper = new DataReaderMapper<T>(reader);
                    do
                    {
                        yield return mapper.MapFrom(reader);
                    } while (reader.Read());
                }
            }
        }
        public IQueryable<TEntity> ReaderSet<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>().AsQueryable();
        }

        public int SaveChangesWithoutTimeShtamp()
        {
            return base.SaveChanges();
        }

        public DbSet<TEntity> WriterSet<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationDbContext).Assembly);
        }

        public DbSet<Person> Persons { get; set; }
        //public DbSet<Father> Fathers { get; set; }
        //public DbSet<Mother> Mothers { get; set; }
    }

}
