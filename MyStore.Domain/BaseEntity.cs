﻿using System;

namespace MyStore.Domain
{
    public class BaseEntity
    {
        public int Id { get; protected set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDt { get; set; }
        public bool IsDeleted { get; protected set; }

        public void SoftDelete(bool state)
        {
            IsDeleted = state;
        }

    }
}
