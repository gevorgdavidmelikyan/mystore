﻿namespace MyStore.Domain.Enums
{
    public enum Sex
    {
        Male,
        Female
    }
}
