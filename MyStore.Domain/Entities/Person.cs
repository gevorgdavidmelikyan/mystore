﻿using MyStore.Domain.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyStore.Domain.Entities
{
    public class Person : BaseEntity
    {
        public string Name { get; set; }
        public Sex? Sex { get; set; }
        [ForeignKey("Father")]
        public int? FatherId { get; set; }
        public Person Father { get; set; }
        [ForeignKey("Mother")]
        public int? MotherId { get; set; }
        public Person Mother { get; set; }



    }
}
