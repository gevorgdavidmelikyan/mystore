﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using MyStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MyStore.Domain.Interfaces
{
    public interface IApplicationDbContext : IDisposable
    {

        DbSet<TEntity> WriterSet<TEntity>() where TEntity : BaseEntity;
        IQueryable<TEntity> ReaderSet<TEntity>() where TEntity : BaseEntity;
        Task<int> SaveChangesAsync(CancellationToken token = default);
        int SaveChanges();
        int SaveChangesWithoutTimeShtamp();
        DatabaseFacade Database { get; }
        EntityEntry Entry(object entity);
        List<TView> Execute<TView>(string name, params KeyValuePair<string, object>[] parameters) where TView : class, new();

        DbSet<Person> Persons { get; set; }
        //DbSet<Father> Fathers { get; set; }
        //DbSet<Mother> Mothers { get; set; }
    }
}
