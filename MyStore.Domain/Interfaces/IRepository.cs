﻿using MyStore.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace MyStore.Infra.Data.Interfaces
{
    public interface IRepository : IDisposable
    {
        Task<IEnumerable<T>> GetAllAsync<T>(params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity;
        Task<IEnumerable<T>> GetAllAsNoTrackingAsync<T>(params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity;
        Task<IEnumerable<T>> GetByPagingAsNoTrackingAsync<T>(int page = 1, int pageSize = 10, params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity;
        Task<T> GetByIdAsync<T>(long id, params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity;
        Task<T> GetByIdAsNoTrackingAsync<T>(long id, params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity;
        Task<IEnumerable<T>> FilterAsNoTrackingAsync<T>(Expression<Func<T, bool>> query, params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity;
        Task<IEnumerable<T>> FilterAsync<T>(Expression<Func<T, bool>> query, params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity;
        IQueryable<T> FilterAsNoTracking<T>(Expression<Func<T, bool>> query, params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity;

        Task<IEnumerable<T>> FilterWithQueryAsync<T>(Expression<Func<T, bool>> query, Func<IQueryable<T>, IQueryable<T>> includeMembers) where T : BaseEntity;
        Task<T> GetAndCheckWithAssignerAsync<T>(long entityId, string assignerId) where T : BaseEntity;
        Task<T> GetAndCheckWithAssignerWithQueryAsync<T>(long entityId, string assignerId, Func<IQueryable<T>, IQueryable<T>> includeMembers) where T : BaseEntity;

        IQueryable<T> Filter<T>(Expression<Func<T, bool>> query,
            params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity;
        IQueryable<T> FilterWithQuery<T>(Expression<Func<T, bool>> query, Func<IQueryable<T>, IQueryable<T>> includeMembers) where T : BaseEntity;
        IQueryable<T> GetAll<T>(params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity;
        IQueryable<T> GetAllAsNoTracking<T>(params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity;
        //List<TView> Execute<TView>(string name, params KeyValuePair<string, object>[] parameters) where TView : new();

        Task<T> Create<T>(T entity) where T : BaseEntity;
        Task<bool> Remove<T>(long id) where T : BaseEntity;
        Task<bool> RemoveRange<T>(IList<long> ids) where T : BaseEntity;
        Task<bool> HardRemove<T>(long id) where T : BaseEntity;
        Task<bool> HardRemoveRange<T>(IList<long> ids) where T : BaseEntity;
        Task<bool> Update<T>(T entity) where T : BaseEntity;
        Task<bool> UpdateRange<T>(IEnumerable<T> entities) where T : BaseEntity;
        Task<bool> CreateRange<T>(IList<T> entities) where T : BaseEntity;
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}
