﻿using MediatR;
using MyStore.Domain.Entities;
using MyStore.Domain.Enums;
using MyStore.Domain.Exeptions;
using MyStore.Infra.Data.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace MyStore.Aplication.Persons.Commands.CreatePerson
{
    public class CreatePersonCommand : IRequest<bool>
    {
        public string Name { get; set; }
        public int FatherId { get; set; }
        public int MotherId { get; set; }
        public Sex Sex { get; set; }
    }
    public class CreatePersonCommandHandler : IRequestHandler<CreatePersonCommand, bool>
    {
        private readonly IRepository _repository;
        public CreatePersonCommandHandler(IRepository repository)
        {
            _repository = repository;
        }
        public async Task<bool> Handle(CreatePersonCommand request, CancellationToken cancellationToken)
        {
            Person father = null;
            Person mother = null;
            if (request.FatherId > 0)
            {
                father = await _repository.GetByIdAsync<Person>(request.FatherId);
                if (father == null)
                {
                    throw new SmartException("father not found");
                }
            }
            if (request.MotherId > 0)
            {
                mother = await _repository.GetByIdAsync<Person>(request.MotherId);
                if (mother == null)
                {
                    throw new SmartException("mother not found");
                }
            }

            await _repository.Create(new Person
            {
                Name = request.Name,
                Sex = request.Sex,
                FatherId = father == null ? 0 : father.Id,
                MotherId = mother == null ? 0 : mother.Id
            });

            return await _repository.SaveChangesAsync() == 1;
        }
    }
}
