﻿using MyStore.Domain.Enums;

namespace MyStore.Aplication.Persons.Queries
{
    public class PersonInfo
    {
        public int PersonId { get; set; }
        public string Name { get; set; }
        public Sex? Sex { get; set; }
        public int? FatherId { get; set; }
        public int? Motherid { get; set; }
    }
}
