﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using MyStore.Domain.Entities;
using MyStore.Infra.Data.Interfaces;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MyStore.Aplication.Persons.Queries
{
    public class GetPersonInfoQuery : IRequest<PersonInfo>
    {
        public int Personid { get; set; }
    }
    public class GetPersonInfoQueryHandler : IRequestHandler<GetPersonInfoQuery, PersonInfo>
    {
        private readonly IRepository _repository;
        public GetPersonInfoQueryHandler(IRepository repository)
        {
            _repository = repository;
        }
        public async Task<PersonInfo> Handle(GetPersonInfoQuery request, CancellationToken cancellationToken)
        {
            var info = await _repository.Filter<Person>(p => p.Id == request.Personid).Select(pi => new PersonInfo
            {
                Name = pi.Name,
                Sex = pi.Sex,
                FatherId = pi.FatherId,
                Motherid = pi.MotherId
            }).FirstOrDefaultAsync();

            return info;
        }
    }
}
