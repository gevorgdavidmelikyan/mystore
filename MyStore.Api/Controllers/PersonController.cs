﻿using Microsoft.AspNetCore.Mvc;
using MyStore.Aplication.Persons.Commands.CreatePerson;
using MyStore.Aplication.Persons.Queries;
using System.Threading.Tasks;

namespace MyStore.Api.Controllers
{

    public class PersonController : BaseController
    {
        [HttpGet]
        public async Task<PersonInfo> Get(int id)
        {
            return await Mediator.Send(new GetPersonInfoQuery { Personid = id });
        }
        [HttpPost]
        public async Task<bool> Create(CreatePersonCommand command)
        {
            return await Mediator.Send(command);
        }
    }
}
